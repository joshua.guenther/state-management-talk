// definition
var LightbulbMachine = /** @class */ (function () {
    function LightbulbMachine(initialState, transitions) {
        this.state = initialState;
        this.transitions = transitions;
    }
    LightbulbMachine.prototype.transition = function (transitionName) {
        var nextState = this.transitions[this.state][transitionName];
        if (!nextState)
            throw new Error("invalid: " + this.state + " -> " + transitionName);
        this.state = nextState;
        console.log("----State----" + "\n", this.state);
    };
    return LightbulbMachine;
}());
// usage
var transitions = {
    lit: { toggle: 'unlit', replace: 'lit' },
    unlit: { toggle: 'lit', replace: 'unlit' },
    broken: { toggle: 'broken', replace: 'unlit' }
};
var betterBulb = new LightbulbMachine('unlit', transitions);
betterBulb.transition('toggle');
betterBulb.transition('toggle');
betterBulb.transition('break');
betterBulb.transition('toggle');
betterBulb.transition('replace');
betterBulb.transition('toggle');
