function undoable(reducer) {
  // Call the reducer with empty action to populate the initial state
  const initialState = {
    past: [],
    present: getInitialState(),
    future: []
  }

  // Return a reducer that handles undo and redo
  return function (state = initialState, action) {
    const { past, present, future } = state

    switch (action.type) {
      case 'UNDO':
        const previous = past[past.length - 1]
        const newPast = past.slice(0, past.length - 1)
        return {
          past: newPast,
          present: previous,
          future: [present, ...future]
        }
      case 'REDO':
        const next = future[0]
        const newFuture = future.slice(1)
        return {
          past: [...past, present],
          present: next,
          future: newFuture
        }
      default:
        // Delegate handling the action to the passed reducer
        const newPresent = reducer(present, action)
        if (present === newPresent) {
          return state
        }
        return {
          past: [...past, present],
          present: newPresent,
          future: []
        }
    }
  }
}



const createStore = (reducer, initialState) => {
  const store = {};
  store.state = reducer(undefined, {});
  store.listeners = [];

  store.getState = () => store.state;

  store.subscribe = listener => {
    store.listeners.push(listener);
  };

  store.dispatch = action => {
    console.log('> Action', action);
    store.state = reducer(store.state, action);
    console.log(store.state);
    store.listeners.forEach(listener => listener());
  };

  return store;
};

const getInitialState = () => {
  return {
    todos: []
  };
};

const todoReducer = (state, action) => {
  let newState = {...state};

  switch (action.type) {
    case 'ADD_TODO':
      return {
        todos: [
          ...state.todos,
          action.payload
        ]
      }
    case 'REMOVE_TODO':
      newState.todos.splice(action.payload, 1);
      return newState;
    case 'TOGGLE_COMPLETED':
      newState.todos[action.payload].completed = !newState.todos[action.payload].completed
      return newState;
    default:
      return state;
  }
};
undoableTodos = undoable(todoReducer);
const store = createStore(undoableTodos, getInitialState());

const allTodosEl = document.getElementById('allTodos');
const completedCountEl = document.getElementById('completedCount');

// store subscriptions
store.subscribe(()=> {
    const state = store.getState();
    const todos = state.present.todos;
    const count = state.present.todos.length;
    renderTodoList(todos);
    completedCountEl.innerHTML = count;
})

store.subscribe(() => {
  document.getElementById("description").value = '';
})

// dom actions

document.addEventListener('keypress', (e) => {
  if (e.key === 'Enter') {
    console.log('----- Previous state', store.getState());
    store.dispatch({
      type: 'ADD_TODO',
      payload: {
        description: document.getElementById("description").value,
        completed: false
      },
    });
  }  
});

document.addEventListener('click', (e) => {
  if (e.key === 'Enter') {
    store.dispatch({
      type: 'ADD_TODO',
      payload: {
        description: document.getElementById("description").value,
        completed: false
      },
    });
  }  
});

function toggleComplete(el) {
  store.dispatch({
    type: 'TOGGLE_COMPLETED',
    payload: el.id
  })
};

function deleteTodo(el) {
  store.dispatch({
    type: 'REMOVE_TODO',
    payload: el.id
  })
};




function renderTodoList(todos) {

  while (allTodosEl.firstChild) {
    allTodosEl.firstChild.remove();
  }
  todos.forEach((todo, idx) => {
    let todoEl = document.createElement("div")
    todoEl.innerHTML = 
      `<span ${todo.completed ? 'style="text-decoration: line-through"' : ''}>${todo.description}</span>
      <button id=${idx} onclick="deleteTodo(this)">Delete</button>
      <button id=${idx} onclick="toggleComplete(this)">Toggle Complete</button>
      `;
    todoEl.id = idx;
    allTodosEl.appendChild(todoEl);
  })

}