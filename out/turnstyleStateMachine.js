// definition
var StateMachine = /** @class */ (function () {
    function StateMachine(initialState, transitions) {
        this.state = initialState;
        this.transitions = transitions;
    }
    StateMachine.prototype.transition = function (transitionName) {
        var nextState = this.transitions[this.state][transitionName];
        if (!nextState)
            throw new Error("invalid: " + this.state + " -> " + transitionName);
        this.state = nextState;
    };
    return StateMachine;
}());
// usage
var transitions = {
    locked: { coin: 'unlocked', push: 'locked' },
    unlocked: { coin: 'unlocked', push: 'locked' },
};
var machine = new StateMachine('locked', transitions);
machine.transition('coin');
console.log(machine.state); // => 'unlocked'
machine.transition('coin');
console.log(machine.state); // => 'unlocked'
machine.transition('push');
console.log(machine.state); // => 'locked'
//# sourceMappingURL=turnstyleStateMachine.js.map