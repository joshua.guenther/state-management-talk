// interface todo {
//   description: string;
//   completed: boolean;
// }
// interface todoState {
//   todos: todo[]
// }
var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
var createStore = function (reducer, initialState) {
    var store = {};
    store.state = initialState;
    store.listeners = [];
    store.getState = function () { return store.state; };
    store.subscribe = function (listener) {
        store.listeners.push(listener);
    };
    store.dispatch = function (action) {
        console.log('> Action', action);
        store.state = reducer(store.state, action);
        store.listeners.forEach(function (listener) { return listener(); });
    };
    return store;
};
var getInitialState = function () {
    return {
        todos: []
    };
};
var todoReducer = function (state, action) {
    switch (action.type) {
        case 'ADD_TODO':
            return {
                todos: __spreadArrays(state.todos, [
                    action.payload.todo
                ])
            };
        //   const nextState = {
        //     count: state.count + action.payload.count,
        //   };
        //   return nextState;
        // case 'TOGGLE_TODO':
        //     const nextState = {...state};
        default:
            return state;
    }
};
var store = createStore(todoReducer, getInitialState());
var counterElement = document.getElementById('counter');
store.subscribe(function () {
    var state = store.getState();
    var count = state.count;
    counterElement.innerHTML = count;
});
document.addEventListener('click', function () {
    console.log('----- Previous state', store.getState());
    store.dispatch({
        type: 'COUNT',
        payload: {
            count: Math.ceil(Math.random() * 10),
        },
    });
    console.log('+++++ New state', store.getState());
});
// https://www.smashingmagazine.com/2018/01/rise-state-machines/
//# sourceMappingURL=flux.js.map