// definition
var Lightbulb = /** @class */ (function () {
    function Lightbulb() {
        this.isLit = false;
        this.isBroken = false;
    }
    Lightbulb.prototype.state = function () {
        return {
            isLit: this.isLit,
            isBroken: this.isBroken
        };
    };
    Lightbulb.prototype.toggle = function () {
        this.isLit = !this.isLit;
    };
    Lightbulb.prototype.breakBulb = function () {
        this.isBroken = true;
    };
    return Lightbulb;
}());
var lightbulb = new Lightbulb();
lightbulb.toggle();
lightbulb.breakBulb();
console.log(lightbulb.state); // isLit: true, isBroken: true
/* one solution: do a check in the toggle and breakBulb methods:

    public toggle() {
      if (isBroken) {
        isLit = false;
        return;
      }
      this.isLit = !this.isLit;
    }

    public breakBulb() {
      this.isBroken = true;
      this.isLit = false;
    }

*/
//# sourceMappingURL=turnstyleNaiveWay.js.map