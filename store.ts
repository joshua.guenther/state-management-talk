// const createStore = (reducer) => {
//     let state;
//     let listeners;
    
//     const dispatch = (action) => {
//         state = reducer(state, action);
//         listeners.forEach(listener => listener());
//     }
    
//     const subscribe = (listener) => {
//         listeners.push(listener);
//         return () => {
//             listeners = listeners.filter(l => l !== listener);
//         }
//     }
    
//     const getState = () => state;

//     // sets initial state for the store
//     dispatch({});

//     return {getState, dispatch, subscribe }
// }

// const getInitialState = () => ({
//     todoList: [], 
//   });


// // reducer is a pure function that takes a state and an action and returns the new state

// const reducer = (prevState = getInitialState(), action) => {
//     switch (action.type) {
//       case 'ADD_TODO':
//         const nextState = {
//           todoList: [
//             ...prevState.todoList,
//             action.text,
//           ],
//         };
  
//         return nextState;
//       default:
//         return prevState;
//   };
// }

//   const store = createStore(reducer);

//   store.subscribe(()=> {
//       const state = store.getState();
//       const count = state.count;
//   })





// // https://www.smashingmagazine.com/2018/01/rise-state-machines/