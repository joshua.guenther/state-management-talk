// definition

class LightbulbMachine {
    public state;
    public transitions;

    constructor (initialState, transitions) {
      this.state = initialState
      this.transitions = transitions
    }
  
    transition (transitionName) {
      var nextState = this.transitions[this.state][transitionName]
      if (!nextState) throw new Error(`invalid: ${this.state} -> ${transitionName}`)
      this.state = nextState
      console.log("----State----" + "\n", this.state);
    }
  }

  // usage

let transitions =   
  {
    lit: { toggle: 'unlit', replace: 'lit' },
    unlit: { toggle: 'lit', replace: 'unlit', break: 'broken' },
    broken: { toggle: 'broken', replace: 'unlit'}
  };

let betterBulb = new LightbulbMachine('unlit', transitions)
  
  betterBulb.transition('toggle');
  betterBulb.transition('toggle');
  betterBulb.transition('break');
  betterBulb.transition('toggle');
  betterBulb.transition('replace');
  betterBulb.transition('toggle');





