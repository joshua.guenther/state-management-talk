// definition

class Lightbulb {
    isLit = false;
    isBroken = false;

    public state() {
      return {
        isLit: this.isLit,
        isBroken: this.isBroken
      }
    }

    public toggle() {
      this.isLit = !this.isLit;
    }

    public breakBulb() {
      this.isBroken = true;
    }
  }

  const lightbulb = new Lightbulb();
  lightbulb.toggle();
  lightbulb.breakBulb();
  console.log(lightbulb.state);  // isLit: true, isBroken: true

/* 

one solution: do a check in the toggle and breakBulb methods:

    public toggle() {
      if (isBroken) {
        isLit = false;
        return;
      }
      this.isLit = !this.isLit;
    }

    public breakBulb() {
      this.isBroken = true;
      this.isLit = false;
    }

but what about a new requirement that says that you can now replace a lightbulb to fix it? => boolean explosion!!!



*/





